<div x-data>
    <script>
        // Not on initial intersection/display
        wiggle = false

        wiggleHorizontalOrVertical = false
        wiggleTitle = true
        debounceInput = true
        showRelatedImagesFromUnslpash = false

        wiggleMatrix = [
            [ 24,  1.0],
            [  0,  2.0],
            [-24,  1.0],
            [  0,  1.8],
            [ 48,  1.0],
            [  0,  1.6],
            [-48,  1.0],
            [  0,  1.4],
            [ 24,  1.0],
            [  0,  1.2],
            [-24,  1.0],
            [  0,  1.0],
            [ 12,  1.0],
            [  0,  1.0],
            [-12,  1.0],
            [  0,  1.0],
            [  6,  1.0],
            [  0,  1.0],
            [ -6,  1.0],
            [  0,  1.0],
            [  3,  1.0],
            [  0,  1.0],
            [ -3,  1.0],
            [  0,  1.0],
            [ 1.5, 1.0],
            [  0,  1.0],
            [-1.5, 1.0],
            [  0,  1.0]
        ]

        // Yes, this is a lame dog's randomizer, but as a simple one liner good enough to randomize some Emojis! 😻
        function randomizeArray(a) {
            return a.sort(() => Math.random() - 0.5);
        }


        function shakeShakeMoji(e, strength = 1) {
            if (!wiggle || e.wiggles) {
                return
            }

            pm = Popmotion

            pm.animate({
                to: [64, 32, 16, 48, 0],
                duration: 300,
                onUpdate: latest => {
                    e.style.filter = `blur(${latest / 16 * strength}px) saturate(${(latest / 5 * strength) + 1}) opacity(${100 - (latest * strength)}%)`
                },
                onComplete: () => {}
            })

            pm.animate({
                to: wiggleMatrix,
                duration: 1000,
                onPlay: () => {},
                onUpdate: latest => {
                    transformString = `rotate(${latest[0] / 4 * strength}deg)`
                    transformString += wiggleHorizontalOrVertical ? `translatey(${latest[0] * strength}px)` : `translatex(${latest[0] * strength}px)`
                    transformString += ` scale(${latest[1]}, ${latest[1]}`
                    e.style.transform = transformString
                },
                onComplete: () => { e.wiggles = false}
            })

            e.wiggles = true
        }


        function unsplash(e, moji) {
            if (e.splashed) {
                return
            }

            e.splashed = true

            imageDiv = document.getElementById('moji_' + moji.id)
             
            imageDiv.innerHTML = null;

            // https://stackoverflow.com/a/58625542/3804973
            const radius = window.innerWidth <= window.innerHeight ? (window.innerWidth / 2) - 120 : (window.innerHeight / 2) - 120
            const step = (2 * Math.PI) / moji.images.length
            var angle = 0

            moji.images.map((image) => {
                const x = Math.round(radius * Math.cos(angle) + window.innerWidth / 2 - 80) + 'px'
                const y = Math.round(radius * Math.sin(angle) + window.innerHeight / 2 - 80) + 'px'
                angle += step

                imageDiv.innerHTML += `<img id="moji_${moji.id}_${image.id}" class="fixed z-10" src="${image.url}&fm=jpg&q=40&fit=crop&w=200&h=200&fit=" style="left: ${x}; top: ${y}; border: 2px solid #fff; box-shadow: 0 0 6px 0px rgb(0, 0, 0, 0.5); width: 160px; height: 160px">`
            })

            pm = Popmotion

            moji.images.forEach((image, idx, images) => {
                strength = 0.25;

                pm.animate({
                    to: [32, 0],
                    duration: 200,
                    onUpdate: latest => {
                        e = document.getElementById('moji_' + moji.id + '_' + image.id)
                        if (e) {
                            e.style.filter = `blur(${latest / 4 * strength}px) saturate(${(latest / 20 * strength) + 1.25})`
                        }
                    }
                })

                pm.animate({
                    to: wiggleMatrix,
                    duration: 800,
                    onPlay: () => {},
                    onUpdate: latest => {
                        e = document.getElementById('moji_' + moji.id + '_' + image.id)
                        if (e) {
                            strengthScale = latest[1] <= 1 ? latest[1] : (1 + ((latest[1] - 1) / 8))
                            transformString = `rotate(${90 + (idx * (360/images.length))}deg)`
                            transformString += ` translatey(${latest[0] * 0.5 * strength}px)`
                            // transformString += ` scale(${strengthScale}, ${strengthScale}`
                            e.style.transform = transformString
                        }
                    },
                    onComplete: () => {}
                })

                // pm.animate({
                //     from: 0.5,
                //     to: 0.66,
                //     duration: 300,
                //     elapsed: -300,
                //     onUpdate: (latest) => {
                //         e = document.getElementById('moji_' + moji.id + '_' + image.id)
                //         // console.log(imageElement)
                //         // imageElement.style.width = '1000px'
                //         if (e) {
                //             e.style.opacity = `${latest * 150}%`
                //             e.style.transform = `scale(${latest}, ${latest}) rotate(${90 + (idx * (360/images.length))}deg)`
                //         }
                //     },
                //     onComplete: () => { }
                // })
            })
        }


        function unUnsplash(e, moji) {
            imageDiv = document.getElementById('moji_' + moji.id)
            imageDiv.innerHTML = null
            e.splashed = false

            return

            pm = Popmotion

            moji.images.forEach((image) => {
                pm.animate({
                    from: 0.0,
                    to: 1.0,
                    duration: 1000,
                    onUpdate: (latest) => {
                        imageElement = document.getElementById('moji_' + moji.id + '_' + image.id)
                        // console.log(imageElement)
                        // imageElement.style.width = '1000px'
                        if (imageElement) {
                            imageElement.style.opacity = `${100 - (latest * 100)}%`
                            imageElement.style.transform = `scale(${latest * 2}, ${latest * 2})`
                        }
                    },
                    onComplete: () => {
                        imageDiv = document.getElementById('moji_' + moji.id)
                        imageDiv.innerHTML = null;
                        e.splashed = false
                    }
                })
            })
        }


        function checkCenter(e, moji) {
            if (!showRelatedImagesFromUnslpash && e.splashed) {
                unUnsplash(e, moji)
            }

            setTimeout(function() {
                elRect = e.getBoundingClientRect()
                
                if (elRect.top > ((window.innerHeight / 2) - (elRect.height * 1.7)) &&
                    elRect.bottom < ((window.innerHeight / 2) + (elRect.height * 1.7))) {
                    // console.log('center')
                    if (moji.images && showRelatedImagesFromUnslpash) {
                        unsplash(e, moji)
                    }
                } else {
                    if (e.splashed) {
                        unUnsplash(e, moji)
                    }
                }

                checkCenter(e, moji)
            }, 10)
        }

        function preloadImages() {
            Alpine.store('mojis').forEach((moji) => {
                moji.images.forEach((image) => {
                    var cache = new Image();
                    cache.src = `${image.url}&fm=jpg&q=40&fit=crop&w=200&h=200&fit=`
                })
            })
        }
        
        document.addEventListener("alpine:init", () => {
            Livewire.hook('message.processed', (message, component) => {
                if (component.fingerprint.name == 'filter-mojis') {
                    Alpine.store('mojis', JSON.parse(@this.mojis))

                    setTimeout(function() {
                        preloadImages()
                    }, 1000)
                }
            })


        })
    </script>

    <style>
        .toggle-path {
            transition: background 0.3s ease-in-out;
        }

        .toggle-circle {
            top: 0.2rem;
            left: 0.25rem;
            transition: all 0.3s ease-in-out;
        }

        input:checked ~ .toggle-circle {
            transform: translateX(100%);
        }

        input:checked ~ .toggle-path {
            background-color:#f0f0f0;
        }
    </style>

    <div class="flex flex-col justify-around space-y-4 mt-6">
        <h2 class="text-2xl font-semibold tracking-wider text-center text-gray-600">The Happy Wiggling Mojis! 🤗🥳🎉</h2>
                    
        <div class="flex flex-col">
            <label for="toogleWiggleHorizontalOrVertical" class="flex mx-auto cursor-pointer">
                <div class="px-2">Wiggle Mojis Horizontal</div>
                <div class="relative">
                    <input id="toogleWiggleHorizontalOrVertical" type="checkbox" class="hidden" @change="wiggleHorizontalOrVertical = $el.checked" />
                    <div class="toggle-path bg-gray-200 w-9 h-5 rounded-full shadow-inner"></div>
                    <div class="toggle-circle absolute w-3.5 h-3.5 bg-white rounded-full shadow inset-y-0 left-0"></div>
                </div>
                <div class="px-2">or Vertical</div>
            </label>

            <div class="mx-auto mt-1 text-xs font-light">(on entering viewport, so please scroll for some fun)</div>
        </div>

        <div class="mx-auto">
            <label for="toogleWiggleTitle" class="flex items-center cursor-pointer">
                <div class="px-2">Wiggle Title</div>
                <div class="relative">
                    <input id="toogleWiggleTitle" type="checkbox" checked="checked" class="hidden" @change="wiggleTitle = $el.checked" />
                    <div class="toggle-path bg-gray-200 w-9 h-5 rounded-full shadow-inner"></div>
                    <div class="toggle-circle absolute w-3.5 h-3.5 bg-white rounded-full shadow inset-y-0 left-0"></div>
                </div>
            </label>

            {{-- <label for="toogleDebounceInput" class="flex items-center cursor-pointer">
                <div class="px-2">Debounce Input</div>
                <div class="relative">
                    <input id="toogleDebounceInput" type="checkbox" checked class="hidden" @change="debounceInput = $el.checked" />
                    <div class="toggle-path bg-gray-200 w-9 h-5 rounded-full shadow-inner"></div>
                    <div class="toggle-circle absolute w-3.5 h-3.5 bg-white rounded-full shadow inset-y-0 left-0"></div>
                </div>
            </label> --}}
        </div>

        <div class="mx-auto">
            <label for="toggleShowRelatedImagesFromUnslpash" class="flex items-center cursor-pointer">
                <div class="px-2">Show related images from Unsplash</div>
                <div class="relative">
                    <input id="toggleShowRelatedImagesFromUnslpash" type="checkbox" {{-- checked="checked" --}}class="hidden" @change="showRelatedImagesFromUnslpash = $el.checked" />
                    <div class="toggle-path bg-gray-200 w-9 h-5 rounded-full shadow-inner"></div>
                    <div class="toggle-circle absolute w-3.5 h-3.5 bg-white rounded-full shadow inset-y-0 left-0"></div>
                </div>
            </label>
        </div>

        <div class="mx-auto">
            <input
                type="text" placeholder="Filter mojis..."
                x-init="$wire.filter().then( result => { setTimeout(function() { wiggle = true }, 100) } )"
                @input.debounce.300ms="wiggle = false; $wire.filter($el.value).then( result => { setTimeout(function() { wiggle = true }, 100) } )"
            />
        </div>
    </div>

    <div class="mb-96 space-y-24 font-extrabold">
        <template x-for="moji in $store.mojis">
            <div class="flex justify-center">
                <div x-intersect="checkCenter($el, moji); shakeShakeMoji($el)" class="text-7xl" x-text="moji.emoji"></div>
                <div x-intersect="if (wiggleTitle) { shakeShakeMoji($el, 0.5) }" class="self-center justify-items-center ml-6 text-xl text-gray-300 capitalize" x-text="moji.title"></div>
                {{-- <div x-if="$store.mojiState[moji.id]">
                    <template x-for="image in moji.images">
                        <img class="flex self-center" x-bind:src="image.url">
                    </template>
                </div>     --}}
                <div x-bind:id="'moji_' + moji.id" class="relative"></div>
            </div>
        </template>
    </div>
</div>