<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWigglyMojisTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mojis', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('emoji');
            $table->boolean('starred')->default(false); // We use this to initially just show a "starred"/favourited selection
            $table->timestamps();
        });

        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string('moji_id');
            $table->string('width');
            $table->string('height');
            $table->enum('orientation', ['landscape', 'portrait', 'squarish']);
            $table->string('api');
            $table->string('name');
            $table->string('username');
            $table->string('url');
            $table->timestamp('fetched_at');
            $table->timestamps();
        });

        Schema::create('json', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type')->index('type_idx');
            // Or jsonb?!
            $table->json('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mojis');
        Schema::dropIfExists('images');
        Schema::dropIfExists('json');
    }
}
