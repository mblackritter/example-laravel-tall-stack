<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $emojis = json_decode(file_get_contents(resource_path('json/emojis.json')));

        $starred = [
            '😻', '🥩', '🦁', '🌶', '🎉', '🥳', '🍔', '🥦', '🥑', '🌸', '🥃',
            '🦠', '🍄', '🌚', '🏖', '🏩', '🕋', '💻', '💡', '💶', '💎', '🧨',
            '🧻', '📦', '🤯', '🤬', '🤗', '☺️', '👠', '🎩', '🐣'
        ];

        $inserts = [];

        foreach ($emojis as $emoji) {
            $star = in_array($emoji->emoji, $starred) ? true : false;

            $inserts[] = [
                'created_at' => Carbon::now(),
                'title' => $emoji->description,
                'emoji' => $emoji->emoji,
                'starred' => $star
            ];
        }

        DB::table('mojis')->insert($inserts);

        // Maybe this can go, when replacing this functionality in `MojiSeetImage` with a query of all emojis which already have images
        DB::table('json')->insert([
            'type' => 'moji_image_seed_state',
            'data' => '{}'
        ]);


        $images = json_decode(file_get_contents(resource_path('json/images.json')));
        $inserts = [];

        $imageCounter = 1;

        foreach ($images as $image) {
            $inserts[] = [
                'fetched_at' => Carbon::parse($image->created_at),
                'created_at' => Carbon::now(),
                'moji_id' => $image->moji_id,
                'width' => $image->width,
                'height' => $image->height,
                'orientation' => $image->orientation,
                'api' => $image->api,
                'name' => $image->name,
                'username' => $image->username,
                'url' => $image->url
            ];

            $imageCounter++;

            if ($imageCounter % 1000 == 0) {
                DB::table('images')->insert($inserts);
                $inserts = [];
                echo "Inserted " . $imageCounter . " images...\r";
            }
        }

        DB::table('images')->insert($inserts);
        echo "Inserted " . $imageCounter . " images...\n";
    }
}
