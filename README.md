# Example - Laravel TALL Stack - The Happy Wiggling Mojis! 🥳🎉

## Commit Changelog

### [3a11664c](https://gitlab.com/mblackritter/example-laravel-tall-stack/-/tree/3a11664cf28801a8329f7b2e081b71896b8f5b24) - Init [Laravel](https://laravel.com)
  
  `composer create-project laravel/laravel example-laravel-tall-stack`


### [aea885ac](https://gitlab.com/mblackritter/example-laravel-tall-stack/-/tree/aea885ace38034964591f56b84fbab863d6f58c3) - Add [TALL stack](https://tallstack.dev) and [Popmotion](https://popmotion.io)

  `composer require livewire/livewire laravel-frontend-presets/tall` ([TALL preset - GitHub](https://github.com/laravel-frontend-presets/tall))
  
  `php artisan ui tall --auth`
  
  `yarn` (instead of `npm install`)
  
  `yarn hot|development|production` (instead of `npm hot|dev|prod`, so it runs `mix` without involving `npm`)

  `yarn add popmotion` ([Popmotion - GitHub](https://github.com/popmotion/popmotion))

  `yarn add @alpinejs/intersect` ([Alpine.js - Intersect](https://alpinejs.dev/plugins/intersect))


### [bdf460e8](https://gitlab.com/mblackritter/example-laravel-tall-stack/-/tree/bdf460e840eb1368b69ec45fb34520661e66e49a) - Implement example: The Happy Wiggling Mojis! 🥳🎉

  As a simple test case I made use of [Alpine's Intersect Plugin](https://alpinejs.dev/plugins/intersect) to let a list of emojis wiggle as they enter the viewport using [Popmotion](https://popmotion.io/).

  The component `/resources/views/components/mbr-test.blade.php` randomizes the list of emojis, renders the emojis provided from the blade component call and takes care of the viewport visibility check (intersection) and wiggling.

  > **Note**: For the sake of simplicity, I left the JavaScript within the component.

  ```html
  <div {{ $attributes }} x-data="{ emojis: randomizeArray({{ $emojis }}) }">
      <template x-for="emoji in emojis">
          <div x-data="{ shown: false }" x-intersect="shakeShakeMoji($el)" x-text="emoji"></div>
      </template>
  </div>
  ```

  Within the template `/resources/views/components/welcome.blade.php` we call the component and provide the wanted emojis.

  ```html
  <x-mbr-test class="space-y-32 text-7xl font-extrabold mx-auto" _emojis="['😻', '🥩', '🦁', '🌶', '...']" />
  ```


### [2a199b85](https://gitlab.com/mblackritter/example-laravel-tall-stack/-/tree/2a199b852a12bbce00f3be1a4dc4c33fdf2ceaea) - Model and database based Mojis and pure Alpine implementation

  - Mojis now load from models/database, starting with a starred default set and on empty input,
  - they're filterable and filtered server side via Livewire using `$wire.filter($el.value)`, therefore no blade template reload happens,
  - use of the Alpine store and updating the store on component's `message.processed`,
  - shows titles, which ("oh wonder") optionally also wiggle,
  - generally smoother, fixed the issue that some Mojis would just happily continue to wiggle because the animation/movement continuously moves them in and out of the viewport,
  - fixed the issue of first time wiggling on load and after empty results, by deactivating the wiggling on input and reactivation after fetching a new set (plus some delay).


  Seeding the Mojis-DB from `/resources/json/emoji.json` which we took from [Gemoji](https://github.com/github/gemoji):
  [emoji.json](https://github.com/github/gemoji/blob/master/db/emoji.json) and starring a defined set of "hot" Mojis.

  ```php
  $emojis = json_decode(file_get_contents(resource_path('json/emoji.json')));

  $starred = [
      '😻', '🥩', '🦁', '🌶', '🎉', '🥳', '🍔', '🥦', '🥑', '🌸', '🥃',
      '🦠', '🍄', '🌚', '🏖', '🏩', '🕋', '💻', '💡', '💶', '💎', '🧨',
      '🧻', '📦', '🤯', '🤬', '🤗', '☺️', '👠', '🎩', '🐣'
  ];

  foreach ($emojis as $emoji) {
      $star = in_array($emoji->emoji, $starred) ? true : false;

      DB::table('mojis')->insert([
          'title' => $emoji->description,
          'emoji' => $emoji->emoji,
          'starred' => $star
      ]);
  }
  ```


  New Livewire component.
  
  ```php
  class FilterMojis extends Component
  {
      public $mojis;

      public function filter($filterString = null) {
          if ($filterString) {
              $this->mojis = Moji::where('title', 'like', '%' . $filterString . '%')->inRandomOrder()->get()->toJSON();
          } else {
              $this->mojis = Moji::where('starred', true)->inRandomOrder()->get()->toJSON();
          }
      }
   
      public function render()
      {
          return view('livewire.filter-mojis');
      }
  }
  ```


  Major parts of the new Alpine and Alpine store based view.

  ```js
  document.addEventListener("alpine:init", () => {
      Livewire.hook('message.processed', (message, component) => {
          if (component.fingerprint.name == 'filter-mojis') {
              Alpine.store('mojis', JSON.parse(@this.mojis))
          }
      })
  })
  ```

  ```html
      <div class="mx-auto">
          <input
              type="text" placeholder="Filter mojis..."
              x-init="$wire.filter().then( result => { setTimeout(function() { wiggle = true }, 100) } )"
              @input.debounce.300ms="wiggle = false; $wire.filter($el.value).then( result => { setTimeout(function() { wiggle = true }, 100) } )"
          />
      </div>
  </div>

  <div class="flex flex-col justify-around">
      <div class="mx-auto space-y-24 font-extrabold">
          <template x-for="moji in $store.mojis">
              <div class="flex justify-center">
                  <div x-intersect="shakeShakeMoji($el)" class="text-7xl" x-text="moji.emoji"></div>
                  <div x-intersect="if (wiggleTitle) { shakeShakeMoji($el, 0.5) }" class="self-center justify-items-center ml-6 text-xl text-gray-300 capitalize" x-text="moji.title" ></div>
              </div>
          </template>
      </div>
  </div>
  ```


### Show images from Unsplash matching the mojis, incl. implementation of fetch and cache (URLs)

  > DODO: Give insights in seeding the DB with matching images (URLs) for the Mojis from Unsplash, exporting these and seeding on new installs, list details of implementations and show some code - like on updated before.


  ```php

  ```


  ```js

  ```

  ```html

  ```
