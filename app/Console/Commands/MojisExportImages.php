<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use Unsplash;

use App\Models\Image;
use App\Models\Json;

class MojisExportImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mojis:export-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Moji Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $images = Image::get();

        file_put_contents(
            resource_path(
                'json/images.json'
            ),
            preg_replace(
                '/^(  +?)\\1(?=[^ ])/m', '$1',
                json_encode($images, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)
            )
        );

        return Command::SUCCESS;
    }
}
