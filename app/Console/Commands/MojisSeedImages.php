<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use Unsplash;

use App\Models\Moji;
use App\Models\Json;

class MojisSeedImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mojis:seed-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed Moji Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $mojis = Moji::get();

        $starred = [
            '😻', '🥩', '🦁', '🌶', '🎉', '🥳', '🍔', '🥦', '🥑', '🌸', '🥃',
            '🦠', '🍄', '🌚', '🏖', '🏩', '🕋', '💻', '💡', '💶', '💎', '🧨',
            '🧻', '📦', '🤯', '🤬', '🤗', '☺️', '👠', '🎩', '🐣'
        ];

        $mojiImageSeedState = Json::where('type', 'moji_image_seed_state')->first();
        $seedStateData = $mojiImageSeedState->data;
        $seeded = isset($seedStateData->unsplash) ? $seedStateData->unsplash : [];

        Unsplash\HttpClient::init([
            'applicationId' => env('UNSPLASH_APP_ID'),
            // These are not required for image search
            'secret'    => 'YOUR APPLICATION SECRET',
            'callbackUrl'   => 'https://your-application.com/oauth/callback',
            'utmSource' => 'NAME OF YOUR APPLICATION'
        ]);


        // Starred first, so we've images for them showing up asap
        // Order by starred, so we only need one clean run...
        // Taken from [PHP-Sort array based on another array?](https://stackoverflow.com/a/67378189/3804973)

        $starred = array_reverse($starred);

        $mojis = $mojis->sort(function($a, $b) use ($starred) {
            return array_search($b->emoji, $starred) <=> array_search($a->emoji, $starred);
        });


        // As we only have 50 queries/h, just do 1 run (times 3 for orientations) each run
        // every 4 Minutes in `/app/console/Kernel.php`
        $count = 0;

        foreach ($mojis as $moji) {
            if (!in_array($moji->emoji, $seeded)) {
                $orientations = ['landscape', 'portrait', 'squarish'];

                // Store in arrays and write to DB only if all three were fetched,
                // in case of hitting rate limits or errors

                $inserts = [];

                foreach($orientations as $orientation) {
                    $unsplashImages = Unsplash\Search::photos($moji->title, 0, 16, $orientation)->getResults();

                    foreach ($unsplashImages as $unsplashImage) {
                        $inserts[] = [
                            'created_at' => now(),
                            'moji_id' => $moji->id,
                            'width' => $unsplashImage['width'],
                            'height' => $unsplashImage['height'],
                            'orientation' => $orientation,
                            'api' => 'unsplash',
                            'name' => $unsplashImage['user']['name'],
                            'username' => $unsplashImage['user']['username'],
                            'url' => $unsplashImage['urls']['raw']
                        ];
                    }
                }

                DB::table('images')->insert($inserts);

                $count++;

                // We store the state of each Emoji, so we can fetch new ones on updates of `json/emoji.json`
                // But maybe this can go, when replacing this functionality with a query of all emojis which already have images
                $seeded[] = $moji->emoji;
                $seedStateData->unsplash = $seeded;
                $mojiImageSeedState->data = $seedStateData;
                $mojiImageSeedState->save();
            }

            if ($count > 0) {
                return Command::SUCCESS;
            }
        }

        return Command::SUCCESS;
    }
}
