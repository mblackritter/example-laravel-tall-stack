<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moji extends Model
{
    // use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    
    public $timestamps = true;
    
    protected $guarded = ['id'];

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
