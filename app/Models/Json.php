<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Json extends Model
{   
    protected $table = 'json';
    protected $guarded = ['id'];

    protected $casts = [
        'data' => 'object',
    ];
}
