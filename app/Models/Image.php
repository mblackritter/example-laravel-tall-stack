<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    public $timestamps = true;

    protected $guarded = ['id'];

    public function moji()
    {
        return $this->belongsTo(Moji::class);
    }
}
