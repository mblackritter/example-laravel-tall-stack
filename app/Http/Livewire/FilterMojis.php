<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Moji;

class FilterMojis extends Component
{
    public $mojis;

    public function filter($filterString = null) {
        if ($filterString) {
            $mojis = Moji::with(
                ['images' => function ($query) {
                    $query->inRandomOrder();
                }
            ])
            ->where('title', 'like', '%' . $filterString . '%')
            ->get();
        } else {
            $mojis = Moji::inRandomOrder()
            ->with(
                ['images' => function ($query) {
                    // Would make use of [staudenmeir/eloquent-eager-limit](https://github.com/staudenmeir/eloquent-eager-limit)
                    // But that requires disabling strict mode within MariaDB, which we've no control over on simple hosting platforms
                    // $query->inRandomOrder()->limit(16);
                    $query->inRandomOrder();
                }
            ])
            ->where('starred', true)
            ->get();
        }

        // Limit to 16 images...
        $mojis->each(function ($moji, $key) {
            $moji->images->splice(16);
        });

        $this->mojis = $mojis->toJson();
    }
 
    public function render()
    {
        return view('livewire.filter-mojis');
    }
}